﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Security.Cryptography;
using System.Diagnostics;

namespace BlockChain
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public class Block
    {
        public int Index { get; set; }
        public string Data { get; set; }
        public long Timestamp { get; set; }
        public string Hash { get; set; }
        public string PreviousHash { get; set; }
    }

    public partial class MainWindow : Window
    {

        List<Block> BlockChain = new List<Block>();
        public MainWindow()
        {
            InitializeComponent();
            //CreateLoop();
        }

        private void AddList_Click(object sender, RoutedEventArgs e)
        {
            DateTime foo = DateTime.UtcNow;
            string inputString = "0" + "HelloWorld" + ((DateTimeOffset)foo).ToUnixTimeSeconds().ToString();
            testBox.Text = inputString;
            MessageBox.Show(inputString);
            string addHash = GenerateSHA256String(inputString);
            Block block = new Block
            {
                Index = 0,
                Data = "HelloWorld",
                Timestamp = ((DateTimeOffset)foo).ToUnixTimeSeconds(),
                Hash = addHash,
                PreviousHash = "null"
            };
            BlockChain.Add(block);
            CreateStackPanel(block);
        }

        private string GenerateSHA256String(string inputString)
        {
            SHA256 sha256 = SHA256Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha256.ComputeHash(bytes);
            //testBox.Text = Convert.ToBase64String(hash);
            return ToHexString(hash);
        }

        private string ToHexString(byte[] hash)
        {
            StringBuilder result = new StringBuilder(hash.Length * 2);
            for (int i = 0; i < hash.Length; i++)
                result.Append(hash[i].ToString(false ? "X2" : "x2"));
            return result.ToString();
        }

        private void CreateLoop()
        {
            int x = 0;
            while (true)
            {
                string input = "HelloWorld" + x;
                string result = GenerateSHA256String(input);
                Debug.WriteLine(x);
                x++;
                if(result.Substring(0,4) == "0000")
                {
                    MessageBox.Show(result + " Number of X:" + x);
                }
            }
        }

        private void CreateStackPanel(Block test)
        { Border border = new Border {
            BorderThickness = new Thickness(5),
            BorderBrush = new SolidColorBrush(Colors.Pink),
            Child =  new StackPanel
            {
                Children =
                {
                    new TextBlock() { Text = test.Index.ToString() },
                    new TextBlock() { Text = test.Data },
                    new TextBlock() { Text = test.Timestamp.ToString() },
                    new TextBlock() { Text = test.Hash.ToString() },
                    new TextBlock() { Text = test.PreviousHash.ToString() },
                },
            },
        };
            listView.Items.Add(border);
        }
    }

}
